using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model;
using Repository.Interfaces;

namespace Api.Controllers
{
    [ApiController]
    [Route("comments")]
    public class CommentController : ControllerBase
    {
        private readonly ILogger<CommentController> _logger;
        private readonly ICommentRepository commentRepository;
        private readonly IPostRepository postRepository;
        public CommentController(ILogger<CommentController> logger, ICommentRepository commentRepository, IPostRepository postRepository)
        {
            _logger = logger;
            this.commentRepository = commentRepository;
            this.postRepository = postRepository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Comment>> GetAll()
        {
            try
            {
                var comments = commentRepository.GetAll();
                return Ok(comments);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id:guid}")]
        public ActionResult<Comment> Get([FromRoute] Guid id)
        {
            try
            {
                var comment = commentRepository.Get(id);
                return Ok(comment);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult<Comment> Post([FromBody] Comment comment)
        {
            try
            {
                ValidationCommentErrors(comment);

                if (!ModelState.IsValid)
                {
                    return BadRequest(new { errors = GetModalErrorMessages() });
                }

                var commentCreated = commentRepository.Create(comment);
                return Ok(commentCreated);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id:guid}")]
        public IActionResult Put([FromRoute] Guid id, [FromBody] Comment comment)
        {
            try
            {
                ValidationCommentErrors(comment);

                if (!ModelState.IsValid)
                {
                    return BadRequest(new { errors = GetModalErrorMessages() });
                }

                comment.Id = id;

                var commentUpdated = commentRepository.Update(comment);

                return Ok(commentUpdated);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            try
            {
                var comment = commentRepository.Delete(id);
                return Ok(comment);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private void ValidationCommentErrors(Comment comment)
        {
            var post = postRepository.Get(comment.PostId);

            if (post == null)
            {
                ModelState.AddModelError(nameof(comment.PostId), "The PostId does not exist.");
            }
        }
        private List<string> GetModalErrorMessages()
        {
            var errors = new List<string>();

            foreach (var item in ModelState.Values)
            {
                errors.Add(item.Errors[0].ErrorMessage);
            }

            return errors;
        }
    }
}