﻿using Microsoft.EntityFrameworkCore;
using Repository.Interfaces;
using Repository.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, new()
    {
        private readonly BlogContext _blogContext;
        private readonly DbSet<TEntity> _dbSet;

        public BaseRepository(BlogContext blogContext)
        {
            _blogContext = blogContext;
            _dbSet = _blogContext.Set<TEntity>();
        }

        public TEntity Create(TEntity entity)
        {
            try
            {
                _dbSet.Add(entity);
                var result = _blogContext.SaveChanges();
                return entity;
            }
            catch (Exception e)
            {
                throw new Exception(Resources.CreateError, e);
            }

        }

        public bool Delete(Guid id)
        {
            try
            {
                var entity = Get(id);

                _dbSet.Remove(entity);
                var result = _blogContext.SaveChanges();
                return result > 0;
            }
            catch (Exception e)
            {
                throw new Exception(Resources.DeleteError, e);
            }

        }

        public TEntity Get(Guid id)
        {
            try
            {
                return _dbSet.Find(id);
            }
            catch (Exception e)
            {
                throw new Exception(Resources.GetError, e);
            }
        }

        public IEnumerable<TEntity> GetAll()
        {
            try
            {
                return _dbSet
                    .ToList();
            }
            catch (Exception e)
            {
                throw new Exception(Resources.GetError, e);
            }
        }

        public TEntity Update(TEntity entity)
        {
            try
            {
                _blogContext.Entry(entity).State = EntityState.Modified;
                var result = _blogContext.SaveChanges();

                return entity;
            }
            catch (Exception e)
            {
                throw new Exception(Resources.UpdateError, e);
            }
        }
    }
}
