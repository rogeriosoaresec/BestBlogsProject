using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Interfaces;

namespace Repository
{
    public class CommentRepository: BaseRepository<Comment>, ICommentRepository
    {
        private readonly BlogContext _blogContext;

        public CommentRepository(BlogContext blogContext) : base(blogContext)
        {
            _blogContext = blogContext;
        }

        public IEnumerable<Comment> GetByPostId(Guid postId)
        {
            var comments = _blogContext
                .Comments
                .Where(w => w.PostId == postId)
                .AsNoTracking()
                .ToList();
            return comments;
        }
    }
}