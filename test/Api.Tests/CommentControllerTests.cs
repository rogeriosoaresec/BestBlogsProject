using System;
using System.Collections.Generic;
using System.Linq;
using Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Model;
using Moq;
using Repository.Interfaces;
using Xunit;

namespace Api.Tests
{
    public class CommentControllerTests
    {
        [Fact]
        public void GetAll_Returns_Existing_Comments()
        {
            // Arrange
            var expected = GetTestComments();

            var mockPostRepo = new Mock<IPostRepository>();
            var mockCommentRepo = new Mock<ICommentRepository>();
            var comments = mockCommentRepo
                .Setup(repo => repo.GetAll())
                .Returns(expected);

            // Act
            var controller = new CommentController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.GetAll().Result;

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Get_Returns_Existing_Comment()
        {
            // Arrange
            var idComment = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6");
            var expected = GetTestComments().FirstOrDefault(c => c.Id == idComment);

            var mockPostRepo = new Mock<IPostRepository>();
            var mockCommentRepo = new Mock<ICommentRepository>();
            var comments = mockCommentRepo
                .Setup(repo => repo.Get(idComment))
                .Returns(expected);

            // Act
            var controller = new CommentController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.Get(idComment).Result;

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Post_Returns_Created_Comment()
        {
            // Arrange
            var existingPost = GetTestPosts().First();
            var newComment = GetTestComments().First();

            var mockPostRepo = new Mock<IPostRepository>();
            mockPostRepo
                .Setup(rep => rep.Get(It.Is<Guid>(i => i == existingPost.Id)))
                .Returns(existingPost);

            var mockCommentRepo = new Mock<ICommentRepository>();
            var comment = mockCommentRepo
                .Setup(repo => repo.Create(newComment))
                .Returns(newComment);

            // Act
            var controller = new CommentController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.Post(newComment).Result;

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Put_Returns_Updated_Comment()
        {
            // Arrange
            var existingPost = GetTestPosts().First();
            var changesToUpdate = GetTestComments().First();
            var commentId = changesToUpdate.Id;

            var mockPostRepo = new Mock<IPostRepository>();
            mockPostRepo
                .Setup(rep => rep.Get(It.Is<Guid>(i => i == existingPost.Id)))
                .Returns(existingPost);

            var mockCommentRepo = new Mock<ICommentRepository>();
            var comment = mockCommentRepo
                .Setup(repo => repo.Update(changesToUpdate))
                .Returns(changesToUpdate);

            // Act
            var controller = new CommentController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.Put(commentId, changesToUpdate);

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Delete_Returns_Deleted_Comment()
        {
            // Arrange
            var commentToDelete = GetTestComments().First();
            var commentId = commentToDelete.Id;

            var mockPostRepo = new Mock<IPostRepository>();
            var mockCommentRepo = new Mock<ICommentRepository>();
            var comment = mockCommentRepo
                .Setup(repo => repo.Delete(commentId));

            // Act
            var controller = new CommentController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.Delete(commentId);

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        #region "Needed methods"
        private List<Post> GetTestPosts()
        {
            var posts = new List<Post>();

            posts.Add(new Post
            {
                Id = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                CreationDate = new DateTime(2022, 6, 2),
                Title = "Existing post",
                Content = "Good to exist."
            });

            return posts;
        }

        private List<Comment> GetTestComments()
        {
            var comments = new List<Comment>();
            comments.Add(new Comment()
            {
                Id = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                PostId = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                CreationDate = new DateTime(2022, 6, 2),
                Author = "Rogerio Soares",
                Content = "Content to test 1"
            });

            comments.Add(new Comment()
            {
                Id = new Guid("6dc7a04a-f4bc-4edf-8ada-56f534e7820d"),
                PostId = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                CreationDate = new DateTime(2022, 6, 3),
                Author = "Rogerio Ferreira",
                Content = "Content to test 2"
            });

            return comments;
        }
        #endregion
    }
}