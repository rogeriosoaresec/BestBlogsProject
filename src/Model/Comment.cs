using System;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public record Comment
    {
        public Guid Id { get; set; }

        public Guid PostId { get; set; }

        [StringLength(120, ErrorMessage = "The Content is too long, it must be less than {1} characters.")]
        public string Content { get; set; }
        [StringLength(30, ErrorMessage = "The Author name is too long, it must be less than {1} characters.")]
        public string Author { get; set; }

        public DateTime CreationDate { get; set; }
    }
}