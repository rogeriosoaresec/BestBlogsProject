﻿using Model;
using System;
using System.Collections.Generic;

namespace Repository.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetAll();
        TEntity Get(Guid id);
        TEntity Create(TEntity post);
        TEntity Update(TEntity post);
        bool Delete(Guid id);
    }
}
