﻿using Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Model;
using Moq;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Api.Tests
{
    public class PostControllerTests
    {
        [Fact]
        public void GetAll_Returns_Existing_Posts()
        {
            // Arrange
            var expected = GetTestPosts();

            var mockCommentRepo = new Mock<ICommentRepository>();
            var mockPostRepo = new Mock<IPostRepository>();
            var posts = mockPostRepo
                .Setup(repo => repo.GetAll())
                .Returns(expected);

            // Act
            var controller = new PostController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.GetAll().Result;

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Get_Returns_Existing_Posts()
        {
            // Arrange
            var idPost = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6");
            var expected = GetTestPosts().FirstOrDefault(c => c.Id == idPost);

            var mockCommentRepo = new Mock<ICommentRepository>();
            var mockPostRepo = new Mock<IPostRepository>();
            var comments = mockPostRepo
                .Setup(repo => repo.Get(idPost))
                .Returns(expected);

            // Act
            var controller = new PostController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.Get(idPost).Result;

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Post_Returns_Created_Post()
        {
            // Arrange
            var newPost = GetTestPosts().First();

            var mockCommentRepo = new Mock<ICommentRepository>();
            var mockPostRepo = new Mock<IPostRepository>();
            var post = mockPostRepo
                .Setup(repo => repo.Create(newPost))
                .Returns(newPost);

            // Act
            var controller = new PostController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.Post(newPost).Result;

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Put_Returns_Updated_Post()
        {
            // Arrange
            var firstPost = GetTestPosts().First();
            var changesToUpdate = GetTestPosts().Last();
            var postId = firstPost.Id;

            changesToUpdate.Id = postId;

            var mockCommentRepo = new Mock<ICommentRepository>();
            var mockPostRepo = new Mock<IPostRepository>();
            var post = mockPostRepo
                .Setup(repo => repo.Update(changesToUpdate))
                .Returns(changesToUpdate);

            // Act
            var controller = new PostController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.Put(postId, changesToUpdate).Result;

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Delete_Returns_Deleted_Post()
        {
            // Arrange
            var postToDelete = GetTestPosts().First();
            var postId = postToDelete.Id;

            var mockCommentRepo = new Mock<ICommentRepository>();
            var mockPostRepo = new Mock<IPostRepository>();
            var post = mockPostRepo
                .Setup(repo => repo.Delete(postId));

            // Act
            var controller = new PostController(logger: null, commentRepository: mockCommentRepo.Object, postRepository: mockPostRepo.Object);
            var result = controller.Delete(postId);

            // Assert
            var redirectToActionResult = Assert.IsType<OkObjectResult>(result);
        }

        #region "Needed methods"
        private List<Post> GetTestPosts()
        {
            var posts = new List<Post>();

            posts.Add(new Post
            {
                Id = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                CreationDate = new DateTime(2022, 6, 2),
                Title = "Post test 1",
                Content = "Initial content."
            });
            posts.Add(new Post
            {
                Id = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                CreationDate = new DateTime(2022, 6, 3),
                Title = "Post test 2",
                Content = "Another content."
            });

            return posts;
        }

        private List<Comment> GetTestComments()
        {
            var comments = new List<Comment>();
            comments.Add(new Comment()
            {
                Id = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                PostId = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                CreationDate = new DateTime(2022, 6, 2),
                Author = "Rogerio Soares",
                Content = "Content to test 1"
            });

            comments.Add(new Comment()
            {
                Id = new Guid("6dc7a04a-f4bc-4edf-8ada-56f534e7820d"),
                PostId = new Guid("3fa85f64-5717-4562-b3fc-2c963f66afa6"),
                CreationDate = new DateTime(2022, 6, 3),
                Author = "Rogerio Ferreira",
                Content = "Content to test 2"
            });

            return comments;
        }
        #endregion
    }
}
