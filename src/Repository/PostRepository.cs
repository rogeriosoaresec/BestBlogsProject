using System;
using System.Collections.Generic;
using Model;
using Repository.Interfaces;

namespace Repository
{
    public class PostRepository : BaseRepository<Post>, IPostRepository
    {
        private readonly BlogContext _blogContext;

        public PostRepository(BlogContext blogContext) : base(blogContext)
        {
            _blogContext = blogContext;
        }
    }
}