using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model;
using Repository.Interfaces;

namespace Api.Controllers
{
    [ApiController]
    [Route("posts")]
    public class PostController : ControllerBase
    {
        private readonly ILogger<PostController> _logger;
        private readonly IPostRepository postRepository;
        private readonly ICommentRepository commentRepository;

        public PostController(ILogger<PostController> logger, IPostRepository postRepository, ICommentRepository commentRepository)
        {
            _logger = logger;
            this.postRepository = postRepository;
            this.commentRepository = commentRepository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Post>> GetAll()
        {
            try
            {
                var post = postRepository.GetAll();
                return Ok(post);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id:guid}")]
        public ActionResult<Post> Get([FromRoute] Guid id)
        {
            try
            {
                var post = postRepository.Get(id);
                return Ok(post);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public ActionResult<Post> Post([FromBody] Post post)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new { errors = GetModalErrorMessages() });
                }

                var postCreated = postRepository.Create(post);
                return Ok(postCreated);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id:guid}")]
        public ActionResult<Post> Put([FromRoute] Guid id, [FromBody] Post post)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new { errors = GetModalErrorMessages() });
                }

                post.Id = id;

                var postById = postRepository.Update(post);

                return Ok(postById);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id:guid}")]
        public IActionResult Delete([FromRoute] Guid id)
        {
            try
            {
                var post = postRepository.Delete(id);
                return Ok(post);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id:guid}/comments")]
        public ActionResult<IEnumerable<Comment>> GetComments([FromRoute] Guid id)
        {
            try
            {
                var comments = commentRepository.GetByPostId(id);
                return Ok(comments);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private List<string> GetModalErrorMessages()
        {
            var errors = new List<string>();

            foreach (var item in ModelState.Values)
            {
                errors.Add(item.Errors[0].ErrorMessage);
            }

            return errors;
        }
    }
}