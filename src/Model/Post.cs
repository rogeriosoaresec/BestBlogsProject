using System;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public record Post
    {
        public Guid Id { get; set; }

        [StringLength(30, ErrorMessage = "The Title is too long, it must be less than {1} characters.")]
        public string Title { get; set; }
        [StringLength(1200, ErrorMessage = "The Content is too long, it must be less than {1} characters.")]
        public string Content { get; set; }

        public DateTime CreationDate { get; set; }
    }
}